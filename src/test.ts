import * as format from './format'
import { Logger } from './logger/logger.class'


const TEST_PATTERN = 'Hello %1;34c%s%0c, I need %1.3f seconds, and %d minutes. %o'
const TEST_PARAMS = [
  "World",
  32.4234244,
  5,
  {bla: "Foo"}
]


const logger = new Logger({
  label: 'LoggerTest',
  labelStyles: [1,36],
  styleMaps: {
    operators: {
      f: [1,3]
    },
    types: {
      'number': [1]
    }
  },
  target: [
    process.stdout,
    'test.log'
  ]
})

logger.log(TEST_PATTERN, ...TEST_PARAMS)

const child = logger.createChild({
  label: 'child',
  labelStyles: [1,34],
  target: [
    'test.log',
    'test-child.log'
  ]
})
child.log('Was geht ab. %o', [0,1,2,3,4,5])