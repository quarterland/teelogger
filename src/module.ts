import * as Format from './format'
import * as Style from './style'
import * as Operators from './operators'
import * as Config from './config'
import { Logger } from './logger'

export { Format, Style, Operators, Config }

export * from './parser'
export * from './logger'

export function createLogger ( config:Config.ILoggerConfig|string ):Logger {
  if ( 'string' === typeof config ) {
    return createLogger({
      label: config,
      labelStyles: [1]
    })
  }

  return new Logger(config)
}