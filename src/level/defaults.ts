import { LogLevels } from './levels.enum'
import { IPredicate } from './interfaces'
import { LogLevel, LogLevelMap } from './types'

export const defaultMap:LogLevelMap<IPredicate> = {
  error: () => true,
  warn: () => true,
  info: () => true,
  debug: () => process.env.NODE_ENV==='debug',
  trace: () => true
}

export function createPredicate ( value:boolean|IPredicate ):IPredicate {

  if ( 'boolean' === typeof value ) {
    return () => value
  }

  return value

}

export function createMap ( map:Partial<LogLevelMap<boolean|IPredicate>>={} ):LogLevelMap<IPredicate> {
  const output:LogLevelMap<IPredicate> = {}
  Object.keys(map).forEach ( key => {
    output[key] = createPredicate(map[key])
  } )
  return {
    ...defaultMap,
     ...output
  }
}
