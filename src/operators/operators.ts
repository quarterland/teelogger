import { IFormatOperator, IValueFormatter } from './interfaces'
import * as styles from '../style'
import { join, create, match } from '../regex'

export const operators = {
  s: {
    code: 's',
    test: /\%s/,
    format ( value:string ) {
      return value
    }
  },
  c: {
    code: 'c',
    test: /\%([\d+|\;]{1,})c/,
    noArg: true,
    format ( value:string, param:string, index:number ) {
      const codes = param.split(';').map ( code => parseInt(code) )
      return styles.mapCodes(codes)
    }
  },  
  o: {
    code: 'o',
    test: /\%o/,
    format ( value:any ) {
      return JSON.stringify(value,null,'  ')
    }
  },
  f: {
    code: 'f',
    test: /\%(\d*\.\d+)?f/,
    format ( value:any, param:string, index:number ) {
      param = param || '1.2'
      const [ base="1", _prec="0" ] = param.split('.')

      const prec = parseInt(_prec)
      const m = parseInt('1'+'0'.repeat(prec))

      value = parseInt(`${value * m}`,10) / m

      return `${value}`
    }
  },
  d: {
    code: 'd',
    test: /\%d/,
    format ( value:number ) {
      return value
    }
  }  
}
