export interface IFormatParam <O extends string=string, T=any> {

  operator: O

  argument: T

}