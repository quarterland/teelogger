
export function lpad ( value:string|any, length:number, fill:string[1] ) {

  if ( 'string' !== typeof value ) {
    return lpad(`${value}`,length,fill)
  }
  while(value.length < length) {
    value = `${fill}${value}`
  }
  return value
}

export function rpad ( value:string|any, length:number, fill:string[1] ) {

  if ( 'string' !== typeof value ) {
    return lpad(`${value}`,length,fill)
  }
  while(value.length < length) {
    value = `${value}${fill}`
  }
  return value
}
