import { IFormatOperator } from './interfaces';
export declare const list: IFormatOperator<any, any>[];
export declare const re: RegExp;
export declare function testOperator<O extends string[1], T>(operator: IFormatOperator<O, T>, value: string): boolean;
export declare function parse(pattern: string, args: any[]): string;
