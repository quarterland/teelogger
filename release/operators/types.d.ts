import { operators } from './operators';
export declare type Operator = keyof typeof operators;
