export interface IValueFormatter<T> {
    (value: T): string;
    (value: T, arg: string | undefined, index: number): string;
    (value: T, arg?: string | undefined, index?: number): string;
}
export interface IFormatOperator<O = string[1], T = any> {
    code: O;
    test: RegExp | string;
    format: IValueFormatter<T>;
    /** @type {boolean} if true, do not consume arguments */
    noArg?: boolean;
}
