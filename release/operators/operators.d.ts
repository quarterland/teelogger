export declare const operators: {
    s: {
        code: string;
        test: RegExp;
        format(value: string): string;
    };
    c: {
        code: string;
        test: RegExp;
        noArg: boolean;
        format(value: string, param: string, index: number): any;
    };
    o: {
        code: string;
        test: RegExp;
        format(value: any): string;
    };
    f: {
        code: string;
        test: RegExp;
        format(value: any, param: string, index: number): string;
    };
    d: {
        code: string;
        test: RegExp;
        format(value: number): number;
    };
};
