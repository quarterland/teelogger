export declare function create(source: string | RegExp): RegExp;
export declare function join(pattern: (RegExp | string)[]): RegExp;
export declare function match(value: string, regex: RegExp): RegExpExecArray[];
