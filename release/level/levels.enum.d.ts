export declare enum LogLevels {
    error = "error",
    warn = "warn",
    info = "info",
    debug = "debug",
    trace = "trace"
}
