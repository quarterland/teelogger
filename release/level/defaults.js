"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultMap = {
    error: () => true,
    warn: () => true,
    info: () => true,
    debug: () => process.env.NODE_ENV === 'debug',
    trace: () => true
};
function createPredicate(value) {
    if ('boolean' === typeof value) {
        return () => value;
    }
    return value;
}
exports.createPredicate = createPredicate;
function createMap(map = {}) {
    const output = {};
    Object.keys(map).forEach(key => {
        output[key] = createPredicate(map[key]);
    });
    return Object.assign({}, exports.defaultMap, output);
}
exports.createMap = createMap;
