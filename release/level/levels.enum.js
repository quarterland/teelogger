"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LogLevels;
(function (LogLevels) {
    LogLevels["error"] = "error";
    LogLevels["warn"] = "warn";
    LogLevels["info"] = "info";
    LogLevels["debug"] = "debug";
    LogLevels["trace"] = "trace";
})(LogLevels = exports.LogLevels || (exports.LogLevels = {}));
