export declare const LOG_LEVEL_ERROR = "error";
export declare const LOG_LEVEL_WARN = "warn";
export declare const LOG_LEVEL_INFO = "info";
export declare const LOG_LEVEL_DEBUG = "debug";
export declare const LOG_LEVEL_TRACE = "trace";
