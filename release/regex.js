"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function create(source) {
    if ('string' === typeof source) {
        return new RegExp(source);
    }
    return source;
}
exports.create = create;
function join(pattern) {
    let rg_sources = [];
    for (var i = 0; i < pattern.length; i++) {
        const current = create(pattern[i]);
        rg_sources.push(current.source);
    }
    const src = rg_sources.join('|');
    const rg = new RegExp(src, 'gm');
    return rg;
}
exports.join = join;
function match(value, regex) {
    let matches = [];
    let match;
    let pos = -1;
    while ((match = regex.exec(value)) && !!match && match.index > pos) {
        pos = match.index;
        matches.push(match);
    }
    return matches;
}
exports.match = match;
