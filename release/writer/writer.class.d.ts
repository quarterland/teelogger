/// <reference types="node" />
import { Writable } from 'stream';
export declare class LogWriter {
    private stdout;
    constructor(stdout?: Writable);
    write(output: string | Buffer): Promise<boolean>;
}
