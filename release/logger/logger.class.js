"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Format = require("../format");
const Style = require("../style");
const Operators = require("../operators");
const parser_class_1 = require("../parser/parser.class");
const util = require("util");
const level_1 = require("../level");
const writer_1 = require("../writer");
class Logger {
    constructor(config, parent) {
        this.config = config;
        this.parent = parent;
        this.writer = new writer_1.LogWriter(writer_1.createOutputStream(this.config.target));
        this.label = Style.wrap(this.config.labelStyles, this.config.label, 0);
        this.labelsFromRoot = this.parent ? [...this.parent.labelsFromRoot, this.label] : [this.label];
        this.parser = new parser_class_1.Parser(Operators.list, this.config.styleMaps);
    }
    write(data) {
        this.writer.write(data);
    }
    getLogLevelPredicate(logLevel) {
        const logLevels = this.config.logLevels;
        if ('function' === typeof logLevels) {
            return () => logLevels(logLevel);
        }
        const map = level_1.createMap(logLevels);
        return map[logLevel];
    }
    logLevel(level, format, args) {
        const predicate = this.getLogLevelPredicate(level);
        if (predicate() === false) {
            return;
        }
        const formatted = this.format(format, ...args);
        if (this.config.showLevel !== false) {
            this.write('[' + level.toUpperCase() + ']');
        }
        if (this.config.time !== false) {
            this.write(Style.dim(Format.now()) + ' ');
        }
        this.write(this.labelsFromRoot.join('|') + ' ');
        this.write(formatted);
        this.write('\n');
    }
    format(pattern, ...args) {
        if ('string' !== typeof pattern) {
            return this.format('', pattern, ...args);
        }
        const format = this.parser.parse(pattern, args);
        const formatted = util.format(format, ...args);
        return formatted;
    }
    log(pattern, ...args) {
        this.logLevel('info', pattern, args);
    }
    error(pattern, ...args) {
        this.logLevel('error', pattern, args);
    }
    info(pattern, ...args) {
        this.logLevel('info', pattern, args);
    }
    warn(pattern, ...args) {
        this.logLevel('warn', pattern, args);
    }
    debug(pattern, ...args) {
        this.logLevel('debug', pattern, args);
    }
    trace(pattern, ...args) {
        this.logLevel('trace', pattern, args);
    }
    defaultChildConfig(options) {
        if ('string' === typeof options) {
            return this.defaultChildConfig({ label: options });
        }
        return Object.assign({}, this.config, {
            parent: this.config
        }, options);
    }
    createChild(label) {
        const options = this.defaultChildConfig(label);
        return new Logger(options, this);
    }
}
exports.Logger = Logger;
