import * as Config from '../config';
import { Parser } from '../parser/parser.class';
import { LogLevel, IPredicate } from '../level';
import { LogWriter } from '../writer';
export declare class Logger {
    private config;
    private parent?;
    constructor(config: Config.ILoggerConfig, parent?: Logger);
    protected writer: LogWriter;
    readonly label: string;
    protected labelsFromRoot: string[];
    readonly parser: Parser;
    protected write(data: string): void;
    protected getLogLevelPredicate(logLevel: LogLevel): IPredicate;
    protected logLevel(level: LogLevel, format: string | any, args: any[]): void;
    format(pattern: string | any, ...args: any[]): any;
    log(pattern: string | any, ...args: any[]): void;
    error(pattern: string | any, ...args: any[]): void;
    info(pattern: string | any, ...args: any[]): void;
    warn(pattern: string | any, ...args: any[]): void;
    debug(pattern: string | any, ...args: any[]): void;
    trace(pattern: string | any, ...args: any[]): void;
    defaultChildConfig(options: string | Config.ILoggerConfig): Config.ILoggerConfig;
    createChild(label: string | Config.ILoggerConfig): Logger;
}
