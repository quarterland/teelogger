"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function underline(text) {
    return wrap(4, text);
}
exports.underline = underline;
function bright(text) {
    return wrap(1, text);
}
exports.bright = bright;
function dim(text) {
    return wrap(2, text);
}
exports.dim = dim;
function invert(text) {
    return wrap(7, text);
}
exports.invert = invert;
function italic(text) {
    return wrap(3, text);
}
exports.italic = italic;
function mapCodes(color) {
    if (!Array.isArray(color)) {
        return mapCodes([color]);
    }
    return '\x1b[' + color.join(';') + 'm';
}
exports.mapCodes = mapCodes;
function wrap(color, value, terminate) {
    let out = mapCodes(color);
    if (!value) {
        return out;
    }
    out += value;
    if ('undefined' === typeof terminate) {
        terminate = 0;
    }
    if ('number' === typeof terminate) {
        out += mapCodes(terminate);
    }
    return out;
}
exports.wrap = wrap;
