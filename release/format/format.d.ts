export declare function format(pattern: string | any, ...args: any[]): any;
export declare function parsePattern(pattern: string, args: any[]): string;
