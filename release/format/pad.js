"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function lpad(value, length, fill) {
    if ('string' !== typeof value) {
        return lpad(`${value}`, length, fill);
    }
    while (value.length < length) {
        value = `${fill}${value}`;
    }
    return value;
}
exports.lpad = lpad;
function rpad(value, length, fill) {
    if ('string' !== typeof value) {
        return lpad(`${value}`, length, fill);
    }
    while (value.length < length) {
        value = `${value}${fill}`;
    }
    return value;
}
exports.rpad = rpad;
