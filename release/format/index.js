"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const colors = require("./colors");
exports.colors = colors;
__export(require("./time"));
__export(require("./pad"));
__export(require("./format"));
