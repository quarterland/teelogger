"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pad_1 = require("./pad");
function now() {
    const t = new Date();
    return t.toLocaleTimeString() + '.' + pad_1.lpad(t.getTime() % 1000, 3, '0');
}
exports.now = now;
