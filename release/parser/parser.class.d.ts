import { IFormatOperator } from '../operators';
import { ILoggerStyleMaps } from '../config';
export declare class Parser {
    private operators;
    constructor(operators: IFormatOperator[], styleMaps?: Partial<ILoggerStyleMaps>);
    private styleMaps;
    protected regex: RegExp;
    protected testOperator(operator: IFormatOperator, value: string): boolean;
    protected formatValueByType(value: any, valueType: string): any;
    protected formatValue(value: any, operator: IFormatOperator): any;
    parse(pattern: string, args: any[]): string;
}
